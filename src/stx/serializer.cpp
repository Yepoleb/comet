#include <string>
#include <cassert>
#include <stdint.h>
#include <cstring>
#include <sstream>
#include <iomanip>

#include "serializer.hpp"
#include "stream.hpp"
#include "bytearray.hpp"
#include "string.hpp"



namespace stx {

TextSerializer& operator<<(TextSerializer& ser, const std::string& val)
{
    ser.writeString(val);
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, const char* val)
{
    ser.writeString(val);
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, int64_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, int32_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, int16_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, uint64_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, uint32_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, uint16_t val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, float val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, double val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, long double val)
{
    ser.writeString(std::to_string(val));
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, unsigned char val)
{
    ser.getStream()->write(reinterpret_cast<char*>(&val), 1);
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, char val)
{
    ser.getStream()->write(&val, 1);
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, bool val)
{
    ser << (val ? "true" : "false");
    return ser;
}

TextSerializer& operator<<(TextSerializer& ser, const ByteArray& val)
{
    std::stringstream ss;
    ss << '"';
    ss << std::hex << std::setfill('0') << std::uppercase;
    for (char c : val) {
        if (std::isalnum(c)) {
            ss << std::setw(0) << c;
        } else {
            ss << "\\x" << std::setw(2) << (int)(unsigned char)c;
        }
    }
    ss << '"';
    ser << ss.str();

    return ser;
}


void TextSerializer::writeString(const std::string& str)
{
    assert(stream);
    stream->write(str.data(), str.size());
}

std::string TextSerializer::readString(int64_t size)
{
    assert(stream);
    ByteArray ba = stream->read(size);
    return std::string(ba.data(), ba.size());
}

BaseStream* TextSerializer::getStream()
{
    return stream;
}

}
