#pragma once
#include <memory>

#include "stx/bytearray.hpp"
#include "stx/socket.hpp"
#include "stx/socketserver.hpp"

#include "server.hpp"
#include "gog_pb.pb.h"



struct HandlerResponse
{
    protobufs::gog_pb::Header header;
    stx::ByteArray data;
};

class GogHandler
{
public:
    GogHandler(stx::SocketServer* server, std::shared_ptr<stx::Socket> socket);

    static void spawn(stx::SocketServer* server, std::shared_ptr<stx::Socket> socket);
    void handle();

private:
    bool handleMessage(stx::ByteArray header_size_bytes);
    HandlerResponse handleLibraryInfo(const stx::ByteArray& msg_data);
    HandlerResponse handleAuthInfo(const stx::ByteArray& msg_data);
    HandlerResponse handleSubscribeTopic(const stx::ByteArray& msg_data);
    HandlerResponse handleGetUserStats(const stx::ByteArray& msg_data);
    HandlerResponse handleGetUserAchievements(const stx::ByteArray& msg_data);
    HandlerResponse handleUnlockUserAchievement(const stx::ByteArray& msg_data);
    HandlerResponse handleClearUserAchievement(const stx::ByteArray& msg_data);

    GogServer* m_server;
    std::shared_ptr<stx::Socket> m_socket;
    Token m_session;
};
