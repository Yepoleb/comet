#include <stdint.h>
#include <memory>

#include "stx/bytearray.hpp"
#include "stx/socket.hpp"
#include "stx/socketserver.hpp"
#include "stx/logger.hpp"
#include "stx/path.hpp"

#include "handler.hpp"
#include "server.hpp"
#include "api.hpp"
#include "config.hpp"

#include "gog_pb.pb.h"
#include "comm_serv.pb.h"
#include "webbroker.pb.h"


enum MSG_SORT
{
    SORT_COMM = 1,
    SORT_WEBBROKER = 2
};

enum MSG_TYPE_ALL
{
    UNKNOWN_MESSAGE = 0
};

enum MSG_TYPE_COMM
{
    LIBRARY_INFO_REQUEST = 1,
    LIBRARY_INFO_RESPONSE = 2,
    AUTH_INFO_REQUEST = 3,
    AUTH_INFO_RESPONSE = 4,
    GET_USER_STATS_REQUEST = 15,
    GET_USER_STATS_RESPONSE = 16,
    GET_USER_ACHIEVEMENT_REQUEST = 23,
    GET_USER_ACHIEVEMENT_RESPONSE = 24,
    UNLOCK_USER_ACHIEVEMENT_REQUEST = 25,
    UNLOCK_USER_ACHIEVEMENT_RESPONSE = 26,
    CLEAR_USER_ACHIEVEMENT_REQUEST = 27,
    CLEAR_USER_ACHIEVEMENT_RESPONSE = 28
};

enum MSG_TYPE_WEBBROKER
{
    AUTH_REQUEST = 1,
    AUTH_RESPONSE = 2,
    SUBSCRIBE_TOPIC_REQUEST = 3,
    SUBSCRIBE_TOPIC_RESPONSE = 4,
    MESSAGE_FROM_TOPIC = 5
};


static constexpr stx::Logger logger("Handler");


uint16_t decodeUint16Be(stx::ByteArray bytes)
{
    return (bytes[0] << 8) | bytes[1];
}

stx::ByteArray encodeUint16Be(uint16_t value)
{
    stx::ByteArray bytes(2);
    bytes[0] = value >> 8;
    bytes[1] = value & 0xFF;
    return bytes;
}

constexpr int32_t combinedMsgId(int32_t sort, int32_t type)
{
    return (sort << 16) | type;
}



GogHandler::GogHandler(stx::SocketServer* server, std::shared_ptr<stx::Socket> socket)
{
    m_server = reinterpret_cast<GogServer*>(server);
    m_socket = socket;
}

void GogHandler::spawn(stx::SocketServer* server, std::shared_ptr<stx::Socket> socket)
{
    GogHandler handler(server, socket);
    handler.handle();
}

void GogHandler::handle()
{
    logger.info("Accepted connection from port %", m_socket->getPeer().port);
    while (true) {
        stx::ByteArray header_size_bytes;
        try {
            header_size_bytes = m_socket->read(2);
        } catch (std::system_error) {
            break;
        }
        if (header_size_bytes.empty()) {
            break;
        }
        bool success;
        try {
            success = handleMessage(header_size_bytes);
        } catch (const std::system_error& e) {
            logger.error("Connection error: %", e.what());
            break;
        }
        if (!success) break;
    }
    logger.info("Client disconnected");
}

bool GogHandler::handleMessage(stx::ByteArray header_size_bytes)
{
    uint16_t header_size = decodeUint16Be(header_size_bytes);
    logger.trace("Header size: %", header_size);
    if (header_size == 0) return true;

    stx::ByteArray header_data = m_socket->read(header_size);
    logger.trace("Header data: %", stx::hex(header_data));
    protobufs::gog_pb::Header header;
    header.ParseFromString(stx::str(header_data));
    logger.trace("Message size: %", header.size());

    stx::ByteArray msg_data = m_socket->read(header.size());
    logger.trace("Message data: %", msg_data);
    logger.debug("Handling message %.%", header.sort(), header.type());

    HandlerResponse resp;
    switch (combinedMsgId(header.sort(), header.type()))
    {
    case combinedMsgId(SORT_COMM, LIBRARY_INFO_REQUEST):
        resp = handleLibraryInfo(msg_data);
        break;
    case combinedMsgId(SORT_COMM, AUTH_INFO_REQUEST):
        resp = handleAuthInfo(msg_data);
        break;
    case combinedMsgId(SORT_COMM, GET_USER_STATS_REQUEST):
        resp = handleGetUserStats(msg_data);
        break;
    case combinedMsgId(SORT_COMM, GET_USER_ACHIEVEMENT_REQUEST):
        resp = handleGetUserAchievements(msg_data);
        break;
    case combinedMsgId(SORT_COMM, UNLOCK_USER_ACHIEVEMENT_REQUEST):
        resp = handleUnlockUserAchievement(msg_data);
        break;
    case combinedMsgId(SORT_COMM, CLEAR_USER_ACHIEVEMENT_REQUEST):
        resp = handleClearUserAchievement(msg_data);
        break;
    case combinedMsgId(SORT_WEBBROKER, SUBSCRIBE_TOPIC_REQUEST):
        resp = handleSubscribeTopic(msg_data);
        break;
    default:
        logger.error("No handler for message %.%", header.sort(), header.type());
        return false;
    }

    resp.header.set_size(resp.data.size());
    if (header.has_oseq()) {
        resp.header.SetExtension(protobufs::gog_pb::Response::rseq, header.oseq());
    }
    stx::ByteArray resp_header_data = stx::bytes(resp.header.SerializeAsString());
    uint16_t resp_header_size = resp_header_data.size();
    stx::ByteArray resp_header_size_bytes = encodeUint16Be(resp_header_size);
    logger.debug("Responding with %.%", resp.header.sort(), resp.header.type());

    logger.trace("Response header data: %", stx::hex(resp_header_data));
    logger.trace("Response message data: %", resp.data);
    m_socket->write(resp_header_size_bytes);
    m_socket->write(resp_header_data);
    m_socket->write(resp.data);

    return true;
}

HandlerResponse GogHandler::handleLibraryInfo(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::LibraryInfoRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    protobufs::comm_serv::LibraryInfoResponse resp_msg;
    resp_msg.set_location(stx::expandHome(PEER_PATH));
    resp_msg.set_update_status(3);

    HandlerResponse resp;
    resp.data = stx::bytes(resp_msg.SerializeAsString());

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(LIBRARY_INFO_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleAuthInfo(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::AuthInfoRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    m_session = m_server->tokenFor(msg.client_id(), msg.client_secret());
    logger.trace("Access token: %", m_session.access_token);

    GogApi api(m_session);
    GogUser user = api.getUser();

    protobufs::comm_serv::AuthInfoResponse resp_msg;
    resp_msg.set_refresh_token(m_session.refresh_token);
    resp_msg.set_environment_type(0);
    resp_msg.set_user_id(m_session.user_id.combinedID());
    resp_msg.set_user_name(user.username);
    resp_msg.set_region(0);

    HandlerResponse resp;
    resp.data = stx::bytes(resp_msg.SerializeAsString());

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(AUTH_INFO_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleGetUserStats(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::GetUserStatsRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    GogApi api(m_session);
    std::vector<GogUserStat> userstats = api.getUserStats(GalaxyID(msg.user_id()));

    protobufs::comm_serv::GetUserStatsResponse resp_msg;
    for (const GogUserStat& stat : userstats) {
        protobufs::comm_serv::GetUserStatsResponse_UserStat& stat_msg =
            *resp_msg.add_user_stats();

        stat_msg.set_stat_id(stat.stat_id);
        stat_msg.set_key(stat.stat_key);
        stat_msg.set_value_type((int)stat.stat_type);
        stat_msg.set_window_size(stat.window_size);
        stat_msg.set_increment_only(stat.increment_only);

        if (stat.stat_type == VALUETYPE_INT) {
            stat_msg.set_int_value(stat.value.i);
            stat_msg.set_int_default_value(stat.default_value.i);
            stat_msg.set_int_min_value(stat.min_value.i);
            stat_msg.set_int_max_value(stat.max_value.i);
            stat_msg.set_int_max_change(stat.max_change.i);
        } else if (stat.stat_type == VALUETYPE_FLOAT) {
            stat_msg.set_float_value(stat.value.f);
            stat_msg.set_float_default_value(stat.default_value.f);
            stat_msg.set_float_min_value(stat.min_value.f);
            stat_msg.set_float_max_value(stat.max_value.f);
            stat_msg.set_float_max_change(stat.max_change.f);
        }
    }
    HandlerResponse resp;
    resp.data = stx::bytes(resp_msg.SerializeAsString());

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(GET_USER_STATS_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleGetUserAchievements(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::GetUserAchievementsRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    GogApi api(m_session);
    GogUserAchievementList achievements = api.getUserAchievements(GalaxyID(msg.user_id()));

    protobufs::comm_serv::GetUserAchievementsResponse resp_msg;
    for (const GogUserAchievement& achiev : achievements.items) {
        protobufs::comm_serv::GetUserAchievementsResponse_UserAchievement& msg_item =
            *resp_msg.add_user_achievements();

        msg_item.set_achievement_id(achiev.achievement_id);
        msg_item.set_key(achiev.achievement_key);
        msg_item.set_name(achiev.name);
        msg_item.set_description(achiev.description);
        msg_item.set_image_url_locked(achiev.image_url_locked);
        msg_item.set_image_url_unlocked(achiev.image_url_unlocked);
        msg_item.set_visible_while_locked(achiev.visible_while_locked);
        if (achiev.unlock_time != 0) {
            msg_item.set_unlock_time(achiev.unlock_time);
        }
        msg_item.set_rarity(achiev.rarity);
        msg_item.set_rarity_level_description(achiev.rarity_desc);
        msg_item.set_rarity_level_slug(achiev.rarity_slug);
    }
    resp_msg.set_language(achievements.language);
    resp_msg.set_achievements_mode(achievements.mode);

    HandlerResponse resp;
    resp.data = stx::bytes(resp_msg.SerializeAsString());

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(GET_USER_ACHIEVEMENT_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleUnlockUserAchievement(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::UnlockUserAchievementRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    GogApi api(m_session);
    api.setUserAchievement(m_session.user_id, msg.achievement_id(), msg.time());

    HandlerResponse resp;
    resp.data = stx::ByteArray();

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(UNLOCK_USER_ACHIEVEMENT_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleClearUserAchievement(const stx::ByteArray& msg_data)
{
    protobufs::comm_serv::ClearUserAchievementRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    GogApi api(m_session);
    api.setUserAchievement(m_session.user_id, msg.achievement_id(), 0);

    HandlerResponse resp;
    resp.data = stx::ByteArray();

    resp.header.set_sort(SORT_COMM);
    resp.header.set_type(CLEAR_USER_ACHIEVEMENT_RESPONSE);

    return resp;
}

HandlerResponse GogHandler::handleSubscribeTopic(const stx::ByteArray& msg_data)
{
    protobufs::webbroker::SubscribeTopicRequest msg;
    msg.ParseFromString(stx::str(msg_data));

    protobufs::webbroker::SubscribeTopicResponse resp_msg;
    resp_msg.set_topic(msg.topic());

    HandlerResponse resp;
    resp.data = stx::bytes(resp_msg.SerializeAsString());

    resp.header.set_sort(SORT_WEBBROKER);
    resp.header.set_type(SUBSCRIBE_TOPIC_RESPONSE);

    return resp;
}
