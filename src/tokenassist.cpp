#include <string>
#include <iostream>
#include <regex>

#include "stx/logger.hpp"

#include "tokenassist.hpp"
#include "token.hpp"


const char* LOGIN_INSTRUCTIONS =
    "Please open % in your web browser.\n"
    "After completing the login you will be redirected to a blank page. Copy the\n"
    "full URL starting with https://embed.gog.com/on_login_success and paste it\n"
    "into this window.\n";

static constexpr stx::Logger logger("");



Token tokenAssistant()
{
    std::string auth_url = getAuthUrl(GALAXY_ID);
    logger.info(LOGIN_INSTRUCTIONS, auth_url);

    std::string login_code;
    while (true) {
        std::string login_url;
        std::cout << "Login URL: " << std::flush;
        std::getline(std::cin, login_url);

        std::regex login_code_re("code=([\\w\\-]+)");
        std::smatch match_res;
        if (!std::regex_search(login_url, match_res, login_code_re)) {
            logger.error("Could not find a login code in the provided URL");
            continue;
        }
        login_code = match_res[1];
        break;
    }
    Token token = Token::fromCode(login_code, GALAXY_ID, GALAXY_SECRET);
    logger.info("Successfully requested new token!");
    return token;
}
