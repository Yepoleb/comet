# Comet Daemon

Minimal [GOG Galaxy](https://www.gog.com/galaxy) compatible daemon to allow
users to authorize with games that use the Galaxy service.

## Usage

1. Create the directories `~/.local/share/comet` and `~/.local/share/comet/peer`
2. Create auth token using `cometd create-token`
3. Copy `libGalaxyPeer64.so` or `libGalaxyPeer.so` from a supported game to
   `~/.local/share/comet/peer`. The architecture has to match the one of the
   game executable, so it's best to copy both if available.
3. Run daemon with `cometd`

## Dependencies

* [libcurl](https://curl.haxx.se/libcurl/) (APT: libcurl4)
* [libprotobuf](https://developers.google.com/protocol-buffers/) (APT: libprotobuf10)

## Compiling

    # Install dependencies
    sudo apt install libcurl4-openssl-dev libprotobuf-dev protobuf-compiler

    mkdir Release
    cd Release
    cmake ..
    make

    # Optionally install
    sudo make install

## License

[GPLv3](LICENSE)
